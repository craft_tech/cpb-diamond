define([
        "controllers/pages/home",
        "controllers/pages/result",
        "controllers/pages/article-duliye-223",
    ],
    function(Home, Result, ArticleDuliye223) {
        var pages = [
            {
                routeId: 'home',
                type: 'main',
                landing: true,
                page: function() {
                    return new Home.View({ template: "pages/home" });
                }
            },
            {
                routeId: 'result',
                type: 'main',
                landing: false,
                page: function() {
                    return new Result.View({ template: "pages/result" });
                }
            },
            {
                routeId: 'article-duliye-223',
                type: 'main',
                landing: false,
                page: function() {
                    return new ArticleDuliye223.View({ template: "pages/article-duliye-223" });
                }
            },
        ];
        return pages;
    });