// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {                       

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.article-duliye-223_page'), 0.2, { autoAlpha: 0, onComplete: function() {
                    //
                } }, 0.1);

                // back
                $('.back').on('click', function() {
                    tl.kill();
                    // app.router.goto(context.args[0]);
                    app.router.goto('home');
                });
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {},
        })
        //  Return the module for AMD compliance.
        return Page;
    })
