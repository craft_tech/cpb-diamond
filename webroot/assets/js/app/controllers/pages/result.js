// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {                       

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.result_page'), 0.2, { autoAlpha: 0, onStart: function(){
                    fillInfo();
                }, onComplete: function() {
                    //
                } }, 0.1);

                // 填充信息
                function fillInfo() {
                    switch(app.userCity) {
                        case '南京德基':
                            app.userCity = app.userCity + '二期负一楼中庭';
                            app.userTime = app.userTime + '10:00-21:00';
                            break;
                        case '长沙平和堂':
                            app.userCity = app.userCity + '一楼中庭';
                            app.userTime = app.userTime + '10:00-21:00';
                            break;
                        case '沈阳中兴':
                            app.userCity = app.userCity + '一楼正门口';
                            app.userTime = app.userTime + '10:00-21:00';
                            break;
                        case '上海久光':
                            app.userCity = app.userCity + '一楼正门广场';
                            app.userTime = app.userTime + '10:00-21:00';
                            break;
                        case '成都伊势丹':
                            app.userCity = '成都红星路广场伊势丹门口';
                            app.userTime = app.userTime + '10:00-20:00';
                            break;
                        case '武商广场':
                            app.userCity = app.userCity + '一楼正门口';
                            app.userTime = app.userTime + '10:00-21:00';
                            break;
                        case '南宁百盛':
                            app.userCity = '南宁万象城一楼中庭';
                            app.userTime = app.userTime + '10:00-21:00';
                            break;
                        default:
                            //
                    };
                    // name
                    $('.label-name').text(app.userName);
                    // time
                    $('.label-time').text(app.userTime);
                    // loc
                    $('.label-city').text(app.userCity);
                };
                
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {},
        })
        //  Return the module for AMD compliance.
        return Page;
    })
