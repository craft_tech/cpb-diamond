<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\User;
use app\models\Sharednew;


class SiteController extends Controller
{ 
    public $enableCsrfValidation = false; 
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionSharednew()
    {  
        $bloodContent = $_REQUEST["bloodContent"];
        $openid = $_REQUEST["openid"];
         
        $Sharednew = new Sharednew();
  
        $Sharednew->bloodContent = $bloodContent;
        $Sharednew->openid = $openid;
      
        $Sharednew->date = date("Y-m-d H:i:s");

        $Sharednew->save();  
        $data = array("success" => true, "user_id" => $Sharednew->id);  
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
        return $data; 

    }

    public function actionUser()
    {  
        $name = $_REQUEST["name"];
        $telphone = $_REQUEST["telphone"];
        $address = $_REQUEST["address"];   
         $openid = $_REQUEST["openid"];   
        $bloodContent = $_REQUEST["bloodContent"];    
        $user = new User();
  
        $user->name = $name;
        $user->telphone = $telphone;
        $user->address = $address;
        $user->bloodContent = $bloodContent;
        $user->openid = $openid; 
 
        $user->date = date("Y-m-d H:i:s");

        $user->save(); 

        $data = array("success" => true, "user_id" => $user->id);


        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $data; 

    }


    public function actionShared()
    { 
       
        @header('Content-Type: application/json; charset=UTF-8');
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 

        $openid = $_REQUEST["openid"];
        $shared = $_REQUEST["shared"]; 

        $user = new User(); 

        $user->shared = $shared;
        $user->openid = $openid;
 
        $user->date = date("Y-m-d H:i:s");

        $user->save(); 

        $data = array("success" => true, "user_id" => $user->id);


        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $data; 
    }
        public function actionUpdateshared()
    { 
       
        @header('Content-Type: application/json; charset=UTF-8');
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
        
        $openid = $_REQUEST["openid"];  
        
        $updateshared = User::find()->where(['openid'=>$openid])->one(); //openid

        $updateshared->shared ='1'; // $bloodContent
        $updateshared->save(); 
        $data = array( 
                'updateshared' => $updateshared
            ); 
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
           
        return $data;  
    }

    

     public function actionFetch()
    {  
        $openid = $_REQUEST["openid"];  
        $fetch=User::find()->where(['openid' => $openid])->all(); 
        // $fetch = User::findBySql('SELECT * FROM user where openid='+$openid+'')->one();  
        $data = array( 
                'fetch' => $fetch 
            ); 
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            // $data = array("fetch" => true, "user_id" => $fetch->id);
        return $data;  
    }
     public function actionSearch()
    {  
        $openid = $_REQUEST["openid"];  
        $Search=User::find()->where(['openid' => $openid])->all(); 
        $data = array( 
                'search' => $Search 
            ); 
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
           
        return $data;  
    } 
     public function actionUpdate()
    {  
        $openid = $_REQUEST["openid"];  
        $bloodContent = $_REQUEST["bloodContent"]; 
        $name = $_REQUEST["name"];  
        $telphone = $_REQUEST["telphone"]; 
 

        $update = User::find()->where(['openid'=>$openid])->one(); //openid

        $update->bloodContent =$bloodContent; // $bloodContent
        $update->telphone =$telphone;
        $update->name =$name; // $bloodContent
       
        $update->save(); 
        $data = array( 
                'update' => $bloodContent 
            ); 
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
           
        return $data;  
    } 
     public function actionRefresh()
    {    
       $openid = $_REQUEST["openid"];  
       $refresh = User::find()->where(['openid'=>$openid])->all();
        foreach ($refresh as $value){
            $value->shared = '0';
            $value->save();
        }
        $data = array( 
                'refresh' =>$refresh 
            ); 
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
        return $data;  
    } 


    public function actionPhb()
    { 
      
            $page=$_POST['page']; //获取前台传过来的页码  
            $pageSize=5;  //设置每页显示的条数  
            $start=($page-1)*5; //从第几条开始取记录  
            // 总数
            $count = User::find()->count();
            //  页数
            $totalPage=ceil($count/$pageSize);  

            $user_mobile = User::find()->asArray()->offset($start)->limit($pageSize)->orderby('bloodContent desc')->all(); 
            $bloodContentScore = User::find()->asArray()->offset(2)->limit(1)->orderby('bloodContent desc')->all(); 
            $tel=array();
            $nas=array(); 
             $opid=array(); 
               $blood=array(); 

            foreach($user_mobile as $value){  
                        $name = $value["name"];
                        $telphone = $value["telphone"];  
                        $openid = $value["openid"]; 
                        $bloodContent=$value["bloodContent"]; 
                        $telphone=substr_replace($value["telphone"], '****', 3, 4); 
                         // //判断是否包含中文字符  
                        if(preg_match("/[\x{4e00}-\x{9fa5}]+/u", $name)) {  
                            //按照中文字符计算长度  
                            $len = mb_strlen($name, 'UTF-8');  
                            //echo '中文';  
                            if($len >= 3){  
                                //三个字符或三个字符以上掐头取尾，中间用*代替  
                                $name = mb_substr($name, 0, 1, 'UTF-8') . '*' . mb_substr($name, -1, 1, 'UTF-8');  
                            } elseif($len == 2) {  
                                //两个字符  
                                $name = mb_substr($name, 0, 1, 'UTF-8') . '*';  
                            }  
                        } else {  
                            //按照英文字串计算长度  
                            $len = strlen($name);  
                            //echo 'English';  
                            if($len >= 3) {  
                                //三个字符或三个字符以上掐头取尾，中间用*代替  
                                $name = substr($name, 0, 1) . '*' . substr($name, -1);  
                            } elseif($len == 2) {  
                                //两个字符  
                                $name = substr($name, 0, 1) . '*';  
                            }  
                        }   
                        array_push($tel,$telphone); 
                         array_push($nas,$name);
                         array_push($opid,$openid);
                          array_push($blood,$bloodContent);
                           
                       
                   
            } 
                $data = array(
                'totalPage' => $totalPage,
                'openid' => $opid,
                'tel' => $tel,
                'nas' => $nas,
                'bloodContentScore'=>$blood
            ); 
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            return $data; 
    } 

}
